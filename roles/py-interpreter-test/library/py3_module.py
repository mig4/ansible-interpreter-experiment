#!/usr/bin/python3

import sys

from ansible.module_utils.basic import AnsibleModule


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict()

    # seed the result dict
    result = dict(
        shabang="<unable to read>",
        exec_prefix=sys.exec_prefix,
        executable=sys.executable,
        prefix=sys.prefix,
        version=sys.version,
        msg="OK"
    )

    # the AnsibleModule object is our abstraction of working with Ansible
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # try to read shabang line from current file; this is to see how Ansible
    # modifies it when sending the module to the remote host
    try:
        with open(__file__, "r") as fobj:
            result["shabang"] = fobj.readline().strip()
    except Exception as exc:
        result["msg"] = "caught {} when trying to read {}".format(exc, __file__)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
